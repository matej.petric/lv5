﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV5
{
    class DataConsolePrinter
    {
        public void Print(ProtectionProxyDataset proxyDataset)
        {
            IList<List<string>> data = proxyDataset.GetData();
            if(data!=null)
            {
                Console.WriteLine("Data accessed!");
                foreach (List<string> row in data)
                {
                    Console.WriteLine(string.Join(" ", row));
                }
            }
            else
            {
                Console.WriteLine("Data protected for this user!");
            }
        }
    }
}
