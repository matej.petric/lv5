﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV5
{
    class ShippingService
    {
        private double PricePerKG;


        public ShippingService(double PricePerKilogram)
        {
            PricePerKG = PricePerKilogram;
        }

        public double CalculatePrice(IShipable shipable)
        {

            return PricePerKG * shipable.Weight;
        }
    }
}
