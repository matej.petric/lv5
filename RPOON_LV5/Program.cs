﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV5
{
    class Program
    {
        static void Main(string[] args)
        {
            //DRUGI
            ShippingService shippingService = new ShippingService(1.5);
            Product TV = new Product("Apple TV", 3500, 6.5);
            Product Laptop = new Product("Lenovo", 2100, 1.75);
            Product Phone = new Product("IPhone X", 4800, 0.75);
            Product Shirt = new Product("T-Shirt", 250, 0.15);
            Box MyPersonalStuff = new Box("My stuff");
            Box Electronics = new Box("Electronics");
            Electronics.Add(TV);
            Electronics.Add(Laptop);
            Electronics.Add(Phone);
            MyPersonalStuff.Add(Electronics);
            MyPersonalStuff.Add(Shirt);
            Console.WriteLine(MyPersonalStuff.Description());
            Console.WriteLine("Total shipping price: " + shippingService.CalculatePrice(MyPersonalStuff));
            Console.WriteLine();

            //TRECI
            User me = User.GenerateUser("Matej");
            ProtectionProxyDataset proxyDataset = new ProtectionProxyDataset(me);
            DataConsolePrinter printer = new DataConsolePrinter();
            printer.Print(proxyDataset);
            User Karlo = User.GenerateUser("Karlo");
            proxyDataset.user = Karlo;
            printer.Print(proxyDataset);
            Console.WriteLine();

            //CETVRTI
            LogProxyDataset logProxyDataset = new LogProxyDataset("sensitiveData.csv");
            IList<List<string>> data = logProxyDataset.GetData();
            Console.WriteLine();

            //PETI
            ReminderNote note = new ReminderNote("Change the theme!", new GrayAndCyanTheme());
            note.Show();
            Console.WriteLine();

            //SESTI
            List<string> LV5 = new List<string>();
            LV5.Add("Ana");
            LV5.Add("Ivana");
            LV5.Add("Bruno");
            LV5.Add("Enio");
            LV5.Add("Tijana");
            LV5.Add("Kiki");
            GroupNote groupNote = new GroupNote(LV5, "Popis studenata", "Studenti iz grupe LV4", new GrayAndCyanTheme());
            groupNote.Show();
            Console.WriteLine();
            groupNote.RemoveMember("Bruno");
            groupNote.Theme = new LightTheme();
            groupNote.Show();
            Console.WriteLine();

            //SEDMI
            Notebook notebook = new Notebook(new ClassicWhiteTheme());
            notebook.AddNote(note);
            notebook.AddNote(groupNote);
            notebook.Display();
        }
    }
}
