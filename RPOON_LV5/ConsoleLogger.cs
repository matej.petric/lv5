﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV5
{
    class ConsoleLogger
    {
        private static ConsoleLogger instance;

        private ConsoleLogger() { }

        public static ConsoleLogger GetInstance()
        {
            if(instance==null)
            {
                instance = new ConsoleLogger();
            }
            return instance;
        }

        public void Log()
        {
            Console.WriteLine("Accessing data at: " + DateTime.Now.ToString());
        }
    }
}
