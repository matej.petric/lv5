﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV5
{
    class GroupNote : Note
    {
        private string name;
        private List<string> group;

        public GroupNote(List<string> Group, string Name, string message, ITheme theme) : base(message, theme)
        {
            name = Name;
            group = Group;
        }
        public void AddMember(string Member)
        {
            group.Add(Member);
        }
        public void RemoveMember(string Member)
        {
            group.Remove(Member);
        }
        public override void Show()
        {
            this.ChangeColor();
            Console.WriteLine(name + ":");
            string framedMessage = this.GetFramedMessage();
            Console.WriteLine(framedMessage);
            foreach(string member in group)
            {
                Console.WriteLine(member);
            }
            Console.ResetColor();
        }
    }
}
