﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV5
{
    class ClassicWhiteTheme : ITheme
    {
        public void SetBackgroundColor()
        {
            Console.BackgroundColor = ConsoleColor.White;
        }
        public void SetFontColor()
        {
            Console.ForegroundColor = ConsoleColor.Black;
        }
        public string GetHeader(int width)
        {
            return new string('-', width);
        }
        public string GetFooter(int width)
        {
            return new string('-', width);
        }
    }
}
